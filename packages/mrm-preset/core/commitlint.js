const pkg = require('./pkg');
const npm = require('./npm');

/**
 * @param {{
 *   state: 'present'|'absent',
 *   preset: string,
 * }} options
 */
function commitlint({ state, preset }) {
  pkg.withPackageJson((packageFile) => {
    packageFile.merge({
      husky: {
        hooks: {
          'commit-msg': 'commitlint -E HUSKY_GIT_PARAMS',
        },
      },
    });
    if (state === 'present') {
      packageFile.merge({
        commitlint: {
          extends: [preset],
        },
      });
    } else {
      packageFile.unset('commitlint');
    }
  });

  npm.dependency({
    dev: true,
    name: ['@commitlint/cli', preset],
    state: state,
  });
}

module.exports = {
  commitlint,
};
