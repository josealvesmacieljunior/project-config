/* eslint-disable sort-keys-fix/sort-keys-fix */
const { yaml, packageJson, file } = require('mrm-core');
const project = require('../core/project');

/**
 * @param {any} base
 * @param {any[]} newValues
 */
function arrayPrepend(base, newValues) {
  return newValues.reduceRight((returnValue, newValue) => {
    if (!returnValue.includes(newValue)) {
      return [newValue].concat(returnValue);
    }
    return returnValue;
  }, base);
}

function task() {
  const packageManager = packageJson().get('config.packageManager', file('yarn.lock').exists() ? 'yarn' : 'npm');
  const cacheDir = `.${packageManager}-cache`;
  const gitlabCIConfig = yaml('.gitlab-ci.yml', {
    image: 'node:lts-alpine',
  });

  // Set cache
  gitlabCIConfig.merge({
    cache: {
      paths: ['node_modules/', cacheDir],
    },
  });

  // Set before script
  gitlabCIConfig.set(
    'before_script',
    packageManager === 'yarn'
      ? [`yarn config set cache-folder ${cacheDir}`, 'yarn install --frozen-lockfile']
      : [`npm config set cache "$(pwd)/${cacheDir}"`, 'npm ci']
  );

  //Default build steps
  gitlabCIConfig.set('stages', arrayPrepend(gitlabCIConfig.get('stages', []), ['build', 'test', 'release']));

  //Default build task
  gitlabCIConfig.merge({
    building_app: {
      stage: 'build',
      script: [`${packageManager} run ${project.build}`],
    },
    testing_app: {
      stage: 'test',
      script: [`${packageManager} run ${project.test}`],
    },
    releasing_app: {
      stage: 'release',
      script: [`${packageManager} run ${project.release}`],
      when: 'manual',
      only: ['master'],
    },
  });

  gitlabCIConfig.save();
}

module.exports = task;
