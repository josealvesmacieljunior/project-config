const { packageJson } = require('mrm-core');
const pkg = require('../core/pkg');
const npm = require('../core/npm');
const { gitIgnore } = require('../core/git');
const { eslintIgnore, eslintConfig } = require('../core/eslint');
const project = require('../core/project');

function createESLint({ eslintPreset: eslintPresetDefault = 'eslint:recommended' }) {
  /**
   *
   * @param {{
   *   eslintPreset: string,
   *   eslintRules: Record<string, any>
   * }} config
   */
  function task({ eslintPreset, eslintRules }) {
    const hasTypescript = packageJson().get('devDependencies.typescript');
    const hasJSX = true;
    const hasJSON = true;

    // Should be added first
    gitIgnore('ESLint', ['.eslintcache']);
    eslintIgnore(['node_modules/', 'coverage/', 'build/', '.cache/']);

    // Preset
    npm.dependency({
      dev: true,
      name: eslintPreset,
      state: !eslintPreset.startsWith('eslint:') ? 'present' : 'absent',
    });
    eslintConfig({
      extends: [eslintPreset],
      rules: eslintRules,
    });

    /** @type {Record<string, boolean>} */
    const extsMap = {
      js: true,
      jsx: hasJSX,
      ts: hasTypescript,
      tsx: hasTypescript && hasJSX,
      json: hasJSON,
    };

    const exts =
      ' --ext ' +
      Object.keys(extsMap)
        .filter((ext) => extsMap[ext])
        .map((ext) => '.' + ext)
        .join(',');

    const packageFile = packageJson();
    pkg.script(packageFile, {
      name: project.lint,
      script: 'eslint . --cache' + exts,
      state: 'present',
    });
    pkg.script(packageFile, {
      name: project.format,
      script: 'eslint . --quiet --cache --fix' + exts,
      state: 'present',
    });
    packageFile.save();

    // Dependencies
    npm.dependency({
      dev: true,
      name: ['eslint', 'babel-eslint', 'prettier'],
      state: 'present',
    });
    npm.dependency({
      dev: true,
      name: ['@typescript-eslint/parser', '@typescript-eslint/eslint-plugin'],
      state: hasTypescript ? 'present' : 'absent',
    });
  }

  task.description = 'Setup ESLint';
  task.parameters = {
    eslintPreset: {
      default: eslintPresetDefault,
      message: 'Enter ESLint preset name',
      type: 'input',
    },
    eslintRules: {
      type: 'config',
    },
  };
  return task;
}

module.exports = createESLint({
  eslintPreset: '@koober/eslint-config',
});
