/**
 * Set state of a json value
 *
 * @template T
 * @param {Json} jsonFile
 * @param {{
 *   path: string | string[],
 *   state: 'present'|'absent',
 *   value?: T | ((previousValue: T) => T)
 *   default?: T | (() => T)
 * }} options
 */
function value(jsonFile, { state, path, default: defaultValue, value: nextValue }) {
  if (state === 'present') {
    if (nextValue != null) {
      const resolvedValue =
        typeof nextValue === 'function'
          ? // @ts-ignore
            nextValue(jsonFile.get(path))
          : nextValue;
      if (resolvedValue != null) {
        jsonFile.set(path, resolvedValue);
      }
    }

    if (jsonFile.get(path) == null) {
      const resolvedValue =
        typeof defaultValue === 'function'
          ? // @ts-ignore
            defaultValue()
          : defaultValue;
      if (resolvedValue != null) {
        jsonFile.set(path, resolvedValue);
      }
    }
  } else {
    jsonFile.unset(path);
  }
}

module.exports = {
  value,
};
