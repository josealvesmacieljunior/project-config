/* eslint-disable node/no-extraneous-require */
/*cSpell: disable*/
// @ts-check

// @ts-ignore
const { intersect } = require('semver-intersect');
const jsonFile = require('./jsonFile');
const { packageJson } = require('mrm-core');

/**
 * An empty placeholder for npm script
 */
const emptyScript = ':';

/**
 * @param {(pkg: PackageJson) => void} f
 */
function withPackageJson(f) {
  const packageFile = packageJson();
  f(packageFile);
  packageFile.save();
}

/**
 * @param {PackageJson} packageFile
 * @param {{
 *   name: string,
 *   state: 'present'|'absent'|'default',
 *   script: string,
 * }} options
 */
function script(packageFile, { name, state, script }) {
  if (state === 'absent') {
    packageFile.removeScript(name);
  } else if (state === 'present' || (state === 'default' && !packageFile.getScript(name))) {
    packageFile.setScript(name, script);
  }
}

/**
 *
 * @param {PackageJson} packageFile
 * @param {Record<string, string>} engineVersionMap
 */
function engineMinVersion(packageFile, engineVersionMap) {
  /**
   * @param {string} engineName
   */
  const engineConstraint = (engineName) => {
    const defaultVersion = engineVersionMap[engineName];
    const currentVersion = packageFile.get('engines.' + engineName, defaultVersion);
    try {
      return intersect(currentVersion, defaultVersion);
    } catch (_) {
      return currentVersion; //leave unchanged
    }
  };

  packageFile.merge({
    engines: Object.keys(engineVersionMap).reduce(
      (acc, engineName) => ({
        ...acc,
        [engineName]: engineConstraint(engineName),
      }),
      {}
    ),
  });
}

module.exports = {
  ...jsonFile,
  emptyScript,
  script,
  engineMinVersion,
  withPackageJson,
};
