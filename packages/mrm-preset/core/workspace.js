const { packageJson } = require('mrm-core');

function useWorkspaces() {
  return packageJson().get('config.packageArchetype') === 'workspace';
}
exports.useWorkspaces = useWorkspaces;
