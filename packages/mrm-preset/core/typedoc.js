const { packageJson, json } = require('mrm-core');
const pkg = require('./pkg');
const npm = require('./npm');

/**
 * @param {{
 *   state: 'present'|'absent'
 * }} options
 */
function typedoc({ state }) {
  const packageFile = packageJson();
  const hasWorkspaces = Boolean(packageFile.get('workspaces'));
  const hasTypedoc = state === 'present';

  pkg.withPackageJson((packageFile) => {
    pkg.script(packageFile, {
      name: 'typedoc',
      script: 'typedoc',
      state: !hasTypedoc || hasWorkspaces ? 'absent' : 'present',
    });
  });

  const typedocFile = json('typedoc.json');

  if (hasTypedoc) {
    typedocFile.merge({
      out: 'docs',
      tsconfig: 'tsconfig.json',
      exclude: [
        '**/build/**/*',
        '**/lib/**/*',
        '**/node_modules/**',
        '**/__tests__/*.(ts|tsx)',
        '**/*.(spec|test).(ts|tsx)',
      ],
      excludePrivate: true,
      excludeExternals: true,
      excludeNotExported: true,
      readme: 'README.md',
      theme: 'default',
    });

    /**
     * Otherwise save the file with content
     */
    typedocFile.save();
  } else {
    typedocFile.delete();
  }

  // Dependencies
  npm.dependency({
    dev: true,
    name: ['typedoc' /*, 'typedoc-plugin-external-module-name'*/],
    state: hasTypedoc ? 'present' : 'absent',
  });
  npm.dependency({
    dev: true,
    name: ['typedoc-plugin-lerna-packages'],
    state: hasTypedoc && hasWorkspaces ? 'present' : 'absent',
  });
}

module.exports = {
  typedoc,
};
