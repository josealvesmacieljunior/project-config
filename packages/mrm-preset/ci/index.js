const { packageJson } = require('mrm-core');
const { hasGit } = require('../core/git');

/**
 *
 */
function createCI() {
  /**
   * @param {PackageJson} packageFile
   * @returns 'gitlab'|'circleci'
   */
  function detectProvider(packageFile) {
    return packageFile.get('config.ci');
  }

  /**
   * @param {string} ciProvider
   * @returns {() => void}
   */
  function getProvider(ciProvider) {
    // @ts-ignore
    return {
      gitlab: require('./gitlab'),
    }[ciProvider];
  }

  /**
   *
   */
  function task() {
    const packageFile = packageJson();
    const gitPresent = hasGit();

    if (gitPresent) {
      const ciProviderName = detectProvider(packageFile);
      if (ciProviderName) {
        const ciProvider = getProvider(ciProviderName);
        ciProvider();
      }
    }
    packageFile.save();
  }

  task.description = 'Setup Continuous Integration';
  task.parameters = {};
  return task;
}

module.exports = createCI();
