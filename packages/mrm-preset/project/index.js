const { packageJson, json, makeDirs } = require('mrm-core');
const git = require('../core/git');
const pkg = require('../core/pkg');
const npm = require('../core/npm');
const { vscodeTask } = require('../core/vscode');
const project = require('../core/project');
const mrmPackageJson = require('../package.json');

function task() {
  const packageFile = packageJson();
  const gitSupported = git.hasGit();
  const useWorkspace = packageFile.get('config.packageArchetype') === 'workspace';
  const packageManager = packageFile.get('config.packageManager', 'npm');

  /**
   *
   * @param {string} script
   */
  const npmRun = (script) => {
    switch (script) {
      case project.install:
      case project.test:
        return 'npm ' + script;
      default:
        return 'npm run ' + script;
    }
  };
  /**
   *
   * @param {string} script
   */
  const lernaRun = (script) => 'lerna run ' + script;
  /**
   *
   * @param {string} script
   */
  const npmRunAll = (script) => 'npx npm-run-all -p "' + script + ':*"';

  //prebuild
  pkg.script(packageFile, {
    name: project.pre(project.build),
    script: npmRunAll(project.pre(project.build)),
    state: 'present',
  });
  pkg.script(packageFile, {
    name: project.pre(project.build) + ':empty',
    script: pkg.emptyScript,
    state: 'present',
  });
  pkg.script(packageFile, {
    name: project.pre(project.build) + ':lint',
    script: npmRun(project.lint),
    state: 'present',
  });

  //build & clean
  pkg.script(packageFile, {
    name: project.build,
    script: npmRunAll(project.build),
    state: 'present',
  });
  pkg.script(packageFile, {
    name: project.build + ':empty',
    script: pkg.emptyScript,
    state: 'present',
  });
  pkg.script(packageFile, {
    name: project.build + ':packages',
    script: lernaRun(project.build),
    state: useWorkspace ? 'present' : 'absent',
  });
  pkg.script(packageFile, {
    name: project.clean,
    script: useWorkspace ? lernaRun(project.clean) : pkg.emptyScript,
    state: useWorkspace ? 'present' : 'default',
  });

  //lint
  pkg.script(packageFile, {
    name: project.lint,
    script: pkg.emptyScript,
    state: 'default',
  });
  pkg.script(packageFile, {
    name: project.format,
    script: pkg.emptyScript,
    state: 'default',
  });

  //test
  pkg.script(packageFile, {
    name: project.coverage,
    script: pkg.emptyScript,
    state: 'default',
  });
  pkg.script(packageFile, {
    name: project.test,
    script: useWorkspace ? lernaRun(project.test) : pkg.emptyScript,
    state: useWorkspace ? 'present' : 'default',
  });
  pkg.script(packageFile, {
    name: project.validate,
    script: npmRun(project.build) + ' && ' + npmRun(project.test),
    state: 'present',
  });

  //postinstall
  pkg.script(packageFile, {
    name: project.post(project.install),
    script: npmRunAll(project.post(project.install)),
    state: 'present',
  });
  pkg.script(packageFile, {
    name: project.post(project.install) + ':empty',
    script: pkg.emptyScript,
    state: 'present',
  });
  pkg.script(packageFile, {
    name: project.post(project.install) + ':packages',
    script: 'lerna bootstrap',
    state: useWorkspace ? 'present' : 'absent',
  });

  //rescue
  pkg.script(packageFile, {
    name: project.rescue,
    script: (gitSupported ? 'git clean -fdx' : '') + ';' + packageManager + ' install',
    state: 'present',
  });

  //release
  pkg.script(packageFile, {
    name: project.release,
    script: pkg.emptyScript,
    state: 'default',
  });

  // workspace
  const lernaConfig = json('lerna.json');
  if (useWorkspace) {
    const packages = ['packages/*'];
    packageFile.merge({ workspaces: { packages } });
    const lernaConfig = json('lerna.json', {
      version: packageFile.get('version'),
    });
    lernaConfig.merge({
      command: {
        publish: {
          conventionalCommits: true,
        },
      },
      npmClient: packageManager,
      packages,
      useWorkspaces: useWorkspace,
    });
    lernaConfig.save();
    makeDirs('packages');
  } else {
    packageFile.unset('workspaces');
    lernaConfig.delete();
  }

  //Engine
  pkg.engineMinVersion(
    packageFile,
    Object.assign(
      {
        node: '>=12.x',
        yarn: '>=1.x',
      },
      mrmPackageJson.engines
    )
  );
  packageFile.save();

  // Dependencies
  npm.dependency({
    dev: true,
    name: ['npm-run-all'],
    state: 'present',
  });
  npm.dependency({
    dev: true,
    name: ['lerna'],
    state: useWorkspace ? 'present' : 'absent',
  });

  //VSCode
  vscodeTask({
    group: {
      isDefault: true,
      kind: 'build',
    },
    script: 'build',
    type: 'npm',
  });
  vscodeTask({
    group: {
      isDefault: true,
      kind: 'test',
    },
    script: 'test',
    type: 'npm',
  });
}

task.description = 'Init project';
task.parameters = {};

module.exports = task;
