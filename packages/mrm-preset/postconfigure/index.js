const { packageJson } = require('mrm-core');
const { spawnSync } = require('child_process');
const project = require('../core/project');

function task() {
  const packageFile = packageJson();
  const packageManager = packageFile.get('config.packageManager', 'npm');
  const formatResult = spawnSync(packageManager, ['run', project.format]);
  if (formatResult.status !== 0) {
    console.error('Error: to format code\n', formatResult.output.join(''));
  }
}
task.description = 'Sort scripts';

module.exports = task;
