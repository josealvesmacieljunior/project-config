const { packageJson } = require('mrm-core');
const git = require('../core/git');
const pkg = require('../core/pkg');
const project = require('../core/project');
const { semanticRelease } = require('../core/semanticRelease');

function task() {
  const useWorkspace = packageJson().get('config.packageArchetype') === 'workspace';

  //release
  pkg.withPackageJson((packageFile) => {
    pkg.script(packageFile, {
      name: project.release,
      script: useWorkspace ? 'lerna publish' : semanticRelease.command(),
      state: useWorkspace ? 'present' : 'default',
    });

    // Detect git repository
    pkg.value(packageFile, {
      path: 'repository',
      state: 'present',
      value: () => {
        const gitRepository = git.remoteSync();
        return gitRepository
          ? {
              type: 'git',
              url: gitRepository,
            }
          : undefined;
      },
    });
  });

  // Dependencies
  semanticRelease({
    state: useWorkspace ? 'absent' : 'present',
    preset: '@semantic-release/gitlab-config', //TODO: cherry pick plugins depending on context (github, gitlab, etc)
  });
}

task.description = 'Setup release task';
task.parameters = {};

module.exports = task;
