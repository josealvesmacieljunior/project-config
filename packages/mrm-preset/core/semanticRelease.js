const npm = require('./npm');
const pkg = require('./pkg');

/**
 * @param {{
 *   state: 'present'|'absent',
 *   preset?: string,
 * }} options
 */
function semanticRelease({ state, preset }) {
  npm.dependency({
    dev: true,
    name: [
      //'semantic-release',
      ...(preset ? [preset] : []),
    ],
    state: state,
  });

  pkg.withPackageJson((packageFile) => {
    if (state == 'present') {
      packageFile.merge({
        release: {
          extends: [preset],
        },
      });
    } else {
      packageFile.unset('release');
    }
  });
}
semanticRelease.command = function () {
  return 'npx semantic-release';
};

module.exports = {
  semanticRelease,
};
