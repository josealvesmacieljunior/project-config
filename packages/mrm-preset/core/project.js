/** @type {'install'} */
const install = 'install';
/** @type {'build'} */
const build = 'build';
/** @type {'format'} */
const format = 'format';
/** @type {'lint'} */
const lint = 'lint';
/** @type {'test'} */
const test = 'test';
/** @type {'coverage'} */
const coverage = 'coverage';
/** @type {'clean'} */
const clean = 'clean';
/** @type {'validate'} */
const validate = 'validate';
/** @type {'release'} */
const release = 'release';
/** @type {'rescue'} */
const rescue = 'rescue';

/**
 * @param {string} taskName
 */
function pre(taskName) {
  return 'pre' + taskName;
}

/**
 * @param {string} taskName
 */
function post(taskName) {
  return 'post' + taskName;
}

module.exports = {
  build,
  clean,
  coverage,
  format,
  install,
  lint,
  post,
  pre,
  release,
  rescue,
  test,
  validate,
};
