const pkg = require('../core/pkg');
const npm = require('../core/npm');
const project = require('../core/project');
const { hasGit } = require('../core/git');

function createGitHooks() {
  function task() {
    const gitSupported = hasGit();

    pkg.withPackageJson((packageFile) => {
      packageFile.merge({
        husky: gitSupported
          ? {
              hooks: {
                'pre-commit': 'npm run ' + project.build,
                'pre-push': 'npm run ' + project.validate,
              },
            }
          : undefined,
      });
    });

    npm.dependency({
      dev: true,
      name: ['husky'],
      state: gitSupported ? 'present' : 'absent',
    });
  }

  task.description = 'Setup Git hooks';
  task.parameters = {};
  return task;
}

module.exports = createGitHooks();
