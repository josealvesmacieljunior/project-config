const { json, packageJson } = require('mrm-core');
const { hasGit } = require('../core/git');

/**
 *
 * @param {{
 *   renovatePresetApplication: string
 *   renovatePresetLibrary: string
 * }} config
 */
function createRenovate({ renovatePresetApplication, renovatePresetLibrary }) {
  /**
   *
   * @param {{
   *   renovatePreset: string
   * }} parameters
   */
  function task({ renovatePreset }) {
    const gitSupported = hasGit();

    if (gitSupported) {
      const config = packageJson().get('config', {});
      const packageArchetype = config.packageArchetype || 'library';
      const renovatePresetResolved =
        renovatePreset || (packageArchetype === 'application' ? renovatePresetApplication : renovatePresetLibrary);
      const renovateFile = json('renovate.json');
      renovateFile.merge({
        $schema: 'http://json.schemastore.org/renovate',
        ignorePaths: ['**/node_modules/**'],
      });
      renovateFile.set(
        'extends',
        [renovatePresetResolved].concat(
          renovateFile.get('extends', []).filter(
            /**
             * @param {string} extension
             */
            (extension) => extension !== renovatePresetApplication && extension !== renovatePresetLibrary
          )
        )
      );
      renovateFile.save();
    }
  }

  task.description = 'Setup Renovate';
  task.parameters = {
    renovatePreset: {
      message: 'Enter Renovate preset name',
      type: 'input',
    },
  };
  return task;
}

module.exports = createRenovate({
  renovatePresetApplication: '@koober/renovate-config:application',
  renovatePresetLibrary: '@koober/renovate-config:library',
});
