<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=# Koober Mrm Preset _(${name})_) -->
# Koober Mrm Preset _(@koober/mrm-preset)_
<!-- AUTO-GENERATED-CONTENT:END -->

[![NPM Version][package-version-svg]][package-url]
[![License][license-image]][license-url]

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=> ${description}&unknownTxt= ) -->
> Mrm configuration presets
<!-- AUTO-GENERATED-CONTENT:END -->

## Getting Started - App Installation

## Usage

### 1. Install Preset

Initialize your project using this command :

```bash
# from the root of your project
npx -p mrm -p @koober/mrm-preset mrm bootstrap --preset @koober/mrm-preset
```

This will install in `package.json` the npm scripts

- `mrm` : alias to mrm using `@koober/mrm-preset` by default

- `configure` : runs `npm run mrm -- configure`

### 2. Configure project

After initialization and after any upgrade of `@koober/mrm-preset`

```bash
# from the root of your project
npm run configure
```

## License
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[${license}][license-url] © ${author}) -->
[MIT][license-url] © Lucien Boudy <lucien.boudy@koober.com>
<!-- AUTO-GENERATED-CONTENT:END -->

<!-- VARIABLES -->

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[package-version-svg]: https://img.shields.io/npm/v/${name}.svg?style=flat-square) -->
[package-version-svg]: https://img.shields.io/npm/v/@koober/mrm-preset.svg?style=flat-square
<!-- AUTO-GENERATED-CONTENT:END -->
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[package-url]: https://www.npmjs.com/package/${name}) -->
[package-url]: https://www.npmjs.com/package/@koober/mrm-preset
<!-- AUTO-GENERATED-CONTENT:END -->
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[license-image]: https://img.shields.io/badge/license-${license}-green.svg?style=flat-square) -->
[license-image]: https://img.shields.io/badge/license-MIT-green.svg?style=flat-square
<!-- AUTO-GENERATED-CONTENT:END -->
[license-url]: ../../LICENSE
