// http://eslint.org/docs/user-guide/configuring
module.exports = {
  extends: [require.resolve('./rules/flowtype'), 'prettier/flowtype'],
  settings: {
    flowtype: {
      onlyFilesWithFlowAnnotation: true,
    },
  },
};
